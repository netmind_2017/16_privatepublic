
public class Animal {
	
	//public variable
	public String color;	
	public String name;
	public int age;
	public boolean cola;
	
	//private variable
	private int legs;
	private char gender;
	private String error;
	
	//CONSTRUCTOR
	Animal (String name,String color, int age,boolean cola){
		this.name= name;
		this.color= color;		
		this.age= age;
		this.cola= cola;				
	}
	
	public void SetLegs (int legs) {
		if (legs == 2 || legs == 4)	
		{
			this.legs = legs;
		}else {
			this.error = "ERROR LEGS";
		}
	}
	
	public int GetLegs () {
		return this.legs;
	}
	
	
	public void SetGender(char g){
		if ((g == 'M' ) || (g == 'F' )) {
			this.gender = g;
		}else{
			this.error = "ERROR GENDER";
		}
	}
	
	public int GetGender () {
		return this.gender;
	}
	
	public String GetError () {
		return this.error;
	}
	
	
}


